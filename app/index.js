import clock from "clock";
import document from "document";
import { battery } from "power";
import { preferences } from "user-settings";
import { locale } from "user-settings";
import userActivity from "user-activity";
import * as util from "../common/utils";
import * as messaging from "messaging";

// Update the clock every minute
clock.granularity = "minutes";

// Preferences
const clockPref = preferences.clockDisplay;
let lang = locale.language;
let prefix = lang.substring(0,2);
  if ( (typeof util.monthName[prefix] === 'undefined') || (typeof util.weekday[prefix] === 'undefined') ) {
    prefix = 'en';
  }


// Get a handle on the <text> element
const lblTime = document.getElementById("lblTime");
const lblMins = document.getElementById("lblMins")
const lblDate = document.getElementById("lblDate");
const lblDay = document.getElementById("lblDay")
const lblDayNum = document.getElementById("lblDayNum")
const lblBatt = document.getElementById("lblBatt");
const lblSteps = document.getElementById("lblSteps");
//const lblCals = document.getElementById("lblCals");

// Update the <text> element every tick with the current time
clock.ontick = (evt) => {
  // Update battery
  let bat = util.monoDigits(Math.floor(battery.chargeLevel));
  lblBatt.text = `${bat}` + "%";

  
  // Update clock
  let today = evt.date;
  let hours = today.getHours();
  let day = today.getDate();
  let wday = today.getDay();
  let month = today.getMonth();
  let year = today.getFullYear();

  if (preferences.clockDisplay === "12h") {
    // 12h format
    hours = hours % 12 || 12;
  }
  
  hours = util.zeroPad(hours);
  let mins = util.zeroPad(today.getMinutes());
  lblTime.text = `${hours}`;
  lblMins.text = `${mins}`;
  
  //Update date
  if (prefix === 'en') {
    //lblDate.text = `${util.weekday[prefix][wday].toUpperCase()} ${day}`; 
    lblDay.text = `${util.weekday[prefix][wday].toUpperCase()}`;
    lblDayNum.text = `${day}`
  } else {
    //lblDate.text = `${day}. ${util.weekday[prefix][wday].toUpperCase()}`; 
  }
  
  //Update steps and stats
  updateStats();
}

messaging.peerSocket.onmessage = function(evt) {
  lblTime.style.fill = evt.data.value;
  lblMins.style.fill = evt.data.value;
  console.log("Color change")
}

function updateStats() {
  const metricSteps = "steps";
  const amountSteps = userActivity.today.adjusted[metricSteps] || 0;
  const stepsGoal = userActivity.goals[metricSteps];
  const metricCals = "calories";
  const amountCals = userActivity.today.adjusted[metricCals] || 0;
  const caloriesGoal = userActivity.goals[metricCals];
  const metricActive = "activeMinutes";
  const amountActive = userActivity.today.adjusted[metricActive] || 0;
  const activeGoal = userActivity.goals[metricActive];
  const metricElevation = "elevationGain";
  const amountElevation = userActivity.today.adjusted[metricElevation] || 0
  const elevationGoal = userActivity.goals[metricElevation];
  //let stepString = util.thsdDot(amountSteps, prefix);
  let stepString = util.numString(amountSteps);
  let calString = util.thsdDot(amountCals, prefix);
  
  lblSteps.text = stepString;
  //lblCals.text = calString;

}

