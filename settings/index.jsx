function Colors(props) {
  return (
    <Page>
      <Section
        title={<Text bold align="center">Font Color</Text>}>
        <ColorSelect
          settingsKey="myColor"
          colors={[
            {color: 'white'},
            {color: 'tomato'},
            {color: 'sandybrown'},
            {color: 'gold'},
            {color: 'aquamarine'},
            {color: 'deepskyblue'},
            {color: 'plum'}
          ]}
        />
      </Section>
    </Page>
  );
}

registerSettingsPage(Colors);